'use strict';

new Vue({
    el: '#app',
    data: {
        gameStarted: false,
        showUsernameInput: true,
        showMonsterNameInput: false,
        dataEntered: false,
        targetsToHeal: 4,
        messages: {
            good: ['Ou yeah! Easy-peasy.', 'Yeeeeah, point black.', 'Wow this stroke were amazing. I feel as happy as a clam.', 'Thanks! You made my day.'],
            bad: ['Relax, you carry your heart on your sleeve.', 'Damm!. Are you kidding me?', 'Dont bullshit me and fight!', 'You are like a pain in the ass.'],
            attackUserMessage: '',
            attackMonstermessage: '',
            userMessage: 'Are you ready for the battle?',
            monsterMessage: 'Yeah, I was born ready.'
        },
        user: {
            name: '',
            targets: 0,
            life: 100,
            attackPower: 10,
            defensePower: 10,
            healPower: 10,
            win: false
        },
        monster: {
            name: '',
            targets: 0,
            life: 100,
            attackPower: 10,
            defensePower: 10,
            healPower: 10,
            win: false
        }
    },
    computed: {
        usernameURL: function() {
            var hostURL = 'https://robohash.org/';
            return hostURL + this.user.name;
        },
        monsterNameURL: function() {
            var hostURL = 'https://robohash.org/';
            return hostURL + this.monster.name;
        },
        userLifeBar: function() {
            return this.user.life + '%';
        },
        monsterLifeBar: function() {
            if (this.monster.life > 100) {
                return '100%';
            } else {
                return this.monster.life + '%';
            }
        },
        userTarget: function () {
            if (this.user.targets > this.targetsToHeal - 1) {
                return true;
            } else {
                return false;
            }
        }
    },
    watch: {
        gameStarted: function() {
            if (this.gameStarted === true) {
                this.user.life = 100;
                this.user.defensePower = 10;
                this.user.attackPower = 10;
                this.user.targets = 0;
                this.user.win = false;
                this.monster.win = false;
            }
        },
        'user.life': function() {
            if (this.user.life <= 0) {
                this.user.life = 0;
                this.knockedOut('user');
            }
        },
        'monster.life': function() {
            if (this.monster.life <= 0) {
                this.monster.life = 0;
                this.knockedOut('monster');
            }
        }
    },
    methods: {
        knockedOut: function(character) {
            switch (character) {
                case 'user':
                    this.monster.win = true;
                    this.endGame();
                    break;
                case 'monster':
                    this.user.win = true;
                    this.endGame();
                    break;
                default:
                    break;
            }
        },
        usernameSubmit: function() {
            this.showUsernameInput = false;
            this.showMonsterNameInput = true;
        },
        monsternameSubmit: function() {
            //Random skills
            this.monster.life = 50 + this.getRandomNumber(150);
            this.monster.attackPower = this.getRandomNumber(20);
            this.monster.defensePower = this.getRandomNumber(20);
            this.monster.healPower = this.getRandomNumber(20);
            //Interface
            this.showMonsterNameInput = false;
            this.dataEntered = true;
        },
        getAttackDamage: function(attack, defense) {
            var attackResult = this.getRandomNumber(attack);
            var defenseResult = this.getRandomNumber(defense);
            var result = attackResult - defenseResult;
            
            return {
                attack: attackResult,
                defense: defenseResult,
                result: (result <= 0)? 0: result
            };
        },
        getRandomNumber: function(max) {
            return Math.floor(Math.random() * max);
        },
        startGame: function() {
            this.gameStarted = true;
        },
        endGame: function() {
            this.gameStarted = false;
        },
        rebootGame: function() {
            var defaultValues = {
                name: '',
                targets: 0,
                life: 100,
                attackPower: 10,
                defensePower: 10,
                healPower: 10,
                win: false
            };
            this.gameStarted = false;
            
            Object.assign(this.user, defaultValues);
            Object.assign(this.monster, defaultValues);

            this.showUsernameInput = true;
            this.showMonsterNameInput = false;
            this.dataEntered = false;
            this.messages.attackUserMessage = '';
            this.messages.userMessage = '';
            this.messages.attackMonstermessage = '';
            this.messages.monsterMessage = '';

        },
        attackMessage: function(stroke) {
            return 'attack: ' + stroke.attack + ' defense: ' + stroke.defense + ' result: ' + stroke.result;
        },
        attack: function(whoami, attackPower, defensePower) {
            var stroke = this.getAttackDamage(attackPower, defensePower);
            
            switch (whoami) {
                case 'user':
                    if (stroke.result > 0) {
                        this.messages.userMessage = this.messages.good[this.getRandomNumber(this.messages.good.length)];
                        this.user.targets++;
                    } else {
                        this.messages.userMessage = this.messages.bad[this.getRandomNumber(this.messages.bad.length)];
                        this.user.targets = 0;
                    }
                    this.monster.life = this.monster.life - stroke.result;
                    this.messages.attackUserMessage = this.attackMessage(stroke);        
                    break;
                case 'monster':
                    if (stroke.result > 0) {
                        this.messages.monsterMessage = this.messages.good[this.getRandomNumber(this.messages.good.length)];
                        this.monster.targets++;
                        if (this.monster.targets === this.targetsToHeal) {
                            this.action('heal', 'monster');
                        }
                    } else {
                        this.messages.monsterMessage = this.messages.bad[this.getRandomNumber(this.messages.bad.length)];
                        this.monster.targets = 0;
                    }
                    this.user.life = this.user.life - stroke.result;
                    this.messages.attackMonstermessage = this.attackMessage(stroke);
                    break;
                default:
                    break;
            }
        },
        action: function(action, character) {
            switch (action) {
                case 'attack':
                    this.attack('user', this.user.attackPower, this.monster.defensePower);
                    this.attack('monster', this.monster.attackPower, this.user.defensePower);
                    break;
                case 'special':
                    this.attack('user', this.user.attackPower * 2, this.monster.defensePower);
                    this.attack('monster', this.monster.attackPower * 2, this.user.defensePower);
                    break;
                case 'heal':
                    if (character === 'user') {
                        this.user.life += this.getRandomNumber(this.user.healPower);
                        this.messages.userMessage = "I'm so rigth now.";
                        this.user.targets = 0;
                    } else if(character === 'monster') {
                        this.monster.life += this.getRandomNumber(this.monster.healPower);
                        this.messages.monsterMessage = "I'm so rigth now.";
                        this.monster.targets = 0;
                    }
                    break;
                break;
                case 'giveUp':
                    this.user.life = 0;
                    break;
                case 'reset':
                    this.rebootGame();
                    break;
            
                default:
                    break;
            }
        }
    }
});